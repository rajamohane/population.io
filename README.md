# population.io
A supporting cli application to upload survey data into population.db.

## requirements
- Microsoft Visual Studio 2017, community edition
- Jetbrains Resharper
- Sonar-lint

## dependencies
### Project dependencies
This application uses the population.db hosted in population project, hence this
is included as a project dependency.
### Package dependencies
- EFCore 6+
- EFSqlite
- ExcelPackage